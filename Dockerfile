# syntax=docker/dockerfile:1

FROM adoptopenjdk/openjdk11:alpine-slim

WORKDIR /teste-primo
COPY app/src/main/java/org/raul/App.java src/main/java/org/raul/

WORKDIR src/main/java
RUN javac org/raul/App.java

ENTRYPOINT ["java", "org.raul.App"]
