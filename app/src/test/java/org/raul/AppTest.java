package org.raul;

import org.junit.Test;
import static org.junit.Assert.*;

public class AppTest {

    @Test
    public void correct_one_investment_profit() {
        var oneInvestmentProfit = App.calculateHowMuchJoaoWillProfitWithOnlyOneInvestment();
        assertEquals(oneInvestmentProfit, 3.02, 0.01);
    }

    @Test
    public void correct_thirty_six_investments_profit() {
        var thirtySixInvestmentsProfit = App.calculateHowMuchJoaoWillProfitWithThirtySixWeeklyInvestments();
        assertEquals(thirtySixInvestmentsProfit, 55.56, 0.01);
    }

    @Test
    public void correct_weekly_rate() {
        var weeklyRate = App.calculateWeeklyRate();
        assertEquals(weeklyRate, 0.000826, 0.000001);
    }

}
