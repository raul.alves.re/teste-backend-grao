package org.raul;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class App {

    static final int P = 100;
    static final double SELIC = 4.25;
    static final DecimalFormat decimalFormatter =  new DecimalFormat("R$ ###,###.00");

    public static void main(String[] args) {
        setDecimalFormatterSymbols();

        var oneInvestmentProfit = calculateHowMuchJoaoWillProfitWithOnlyOneInvestment();
        var totalWithOneInvestmentProfit = P + oneInvestmentProfit;
        var thirtySixInvestmentsProfit = calculateHowMuchJoaoWillProfitWithThirtySixWeeklyInvestments();
        var totalWithThirtySixInvestmentsProfit = P * 36 + thirtySixInvestmentsProfit;

        System.out.println("João, em 36 semanas seus R$ 100,00 serão " + decimalFormatter.format(totalWithOneInvestmentProfit));
        System.out.println("Você lucra " + decimalFormatter.format(oneInvestmentProfit) + "\n");

        System.out.println("Caso você invista R$ 100,00 semanalmente, em 36 semanas você terá " + decimalFormatter.format(totalWithThirtySixInvestmentsProfit));
        System.out.println("Você lucra R$ " + decimalFormatter.format(thirtySixInvestmentsProfit));
    }

    static void setDecimalFormatterSymbols() {
        DecimalFormatSymbols decimalFormatterSymbols = new DecimalFormatSymbols(Locale.getDefault());
        decimalFormatterSymbols.setDecimalSeparator(',');
        decimalFormatterSymbols.setGroupingSeparator('.');
        decimalFormatter.setDecimalFormatSymbols(decimalFormatterSymbols);
    }

    static double calculateHowMuchJoaoWillProfitWithOnlyOneInvestment() {
        final double i = SELIC / 100;
        final double t = 36 * 5; // 36 weeks, 5 business days
        final double M = P * Math.pow(1 + i, t / 252);

        return round(M - P,2);
    }

    static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    static double calculateHowMuchJoaoWillProfitWithThirtySixWeeklyInvestments() {
        final double i = calculateWeeklyRate();
        final double M = P * (1 + i) * ((Math.pow(1 + i, 36) - 1) / i);

        return round(M - P * 36, 2);
    }

    static double calculateWeeklyRate() {
        final double i = SELIC / 100;

        return Math.pow(1 + i, 5.0 / 252) - 1;
    }

}
